from django.db import models
from django.forms import ModelForm



class ContactForm (models.Model):

    firstname =models.CharField(max_length=100)
    lastname =models.CharField(max_length=100)
    Email =models.EmailField()
    Message =models.CharField(max_length=1000)

class ContactForm(ModelForm):
    class Meta:
        model = ContactForm
        fields =('lastname','firstname', 'Email', 'Message')
