from django import forms

class ContactForm2 (forms.Form):
    name = forms.CharField(max_length=200)
    firstname = forms.CharField(max_length=200)
    email = forms.CharField(max_length=200)
    message = forms.CharField(max_length=1000)
